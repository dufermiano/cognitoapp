var awsRegion = 'us-east-1';
var awsIdentityPoolId = 'us-east-1:369a7924-94e2-47b5-8632-1850a69c0d4c';
var awsCredentials = new AWS.CognitoIdentityCredentials({
	IdentityPoolId : awsIdentityPoolId
});
var awsCognitoCredentials = new AWS.CognitoIdentityCredentials({
	IdentityPoolId : awsIdentityPoolId
});

AWS.config.region = awsRegion;
AWS.config.credentials = awsCredentials;
AWSCognito.config.credentials = awsCognitoCredentials;
AWSCognito.config.region = awsRegion;

var userPoolId = 'us-east-1_Dd023G1Fg';
var userPoolClientId = '5kgluastnncas0q56h95lpdngr';

var awsPoolData = {
	UserPoolId : userPoolId,
	ClientId : userPoolClientId
};

var awsUserPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(awsPoolData);

function registraUser(event) {

	event.preventDefault();
	var nome = document.getElementById("nome").value;
	var attributeList = [];
	var dataEmail = {
		Name : 'email',
		Value : document.getElementById("email").value
	};
	var attributeEmail = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataEmail);

	attributeList.push(attributeEmail);

	var senha = document.getElementById("senha").value;

	var cognitoUser;
	awsUserPool.signUp(nome, senha, attributeList, null, function(err, result) {
		if (err) {
			alert(err);
			return;
		}
		alert("User sign up successfully!");
		confirma();
	});

}

$(document).ready(function() {
	$('#formregistra').on('submit', function(event) {
		event.preventDefault();
		registraUser(event);
	});
});

function confirma() {
	var codigo = prompt("Enter here your confirmation code!");
	var userData = {
		Username : document.getElementById("nome").value, // your username here
		Pool : awsUserPool
	};

	var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
	cognitoUser.confirmRegistration(codigo, true, function(err, result) {
		if (err) {
			alert(err);
			return;
		}
		autentica();
	});

}

function logoutUser() {
	var user = decodeURIComponent(new RegExp('[\?&]' + 'ClientID' + '=([^&#]*)').exec(window.location.href)[1]);
	var userData = {
		Username : user, // your username here
		Pool : awsUserPool
	};

	var cognitoUser = awsUserPool.getCurrentUser();

	if (cognitoUser != null) {
		cognitoUser.signOut();
		AWS.config.credentials.clearCachedId();
		window.location = "index.html";
		localStorage.removeItem("login");
	}

}

function autentica() {
	var authenticationData = {
		Username : document.getElementById("nome").value,
		Password : document.getElementById("senha").value,
	};
	var authenticationDetails = new AWSCognito.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);
	var userData = {
		Username : document.getElementById("nome").value, // your username here
		Pool : awsUserPool
	};
	var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);

	cognitoUser.authenticateUser(authenticationDetails, {
		onSuccess : function(result) {
			var cognitoUser = awsUserPool.getCurrentUser();

			if (cognitoUser != null) {
				cognitoUser.getSession(function(err, result) {
					if (result) {
						// Add the User's Id Token to the Cognito credentials login map.
						AWS.config.credentials = new AWS.CognitoIdentityCredentials({
							IdentityPoolId : awsIdentityPoolId,
							Logins : {
								'cognito-idp.us-east-1.amazonaws.com/'+userPoolId+' : result.getIdToken().getJwtToken()
							}
						});

						AWS.config.credentials.get(function() {

							window.location = "first.html?ClientID=" + document.getElementById("nome").value + "&token=" + result.getIdToken().getJwtToken();
							localStorage.setItem("login", "cognito");

						});

					}
				});
			}
		},

		onFailure : function(err) {
			alert(err);
		},
	});

}

