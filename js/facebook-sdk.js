function statusChangeCallback(response) {
	if (response.status === 'connected') {

		AWS.config.credentials.get(function() {
			var syncClient = new AWS.CognitoSyncManager();
			syncClient.openOrCreateDataset('myDataset', function(err, dataset) {
				dataset.put('myKey', 'myValue', function(err, record) {
					dataset.synchronize({
						onSuccess : function(data, newRecords) {
							console.log("logou no cognito");
						}
					});
				});
			});
		});

	} else if (response.status === 'not_authorized') {
		alert('Please log ' + 'into this app.');
	} else {
		alert('Please log ' + 'into Facebook.');
	}
}

function checkLoginState() {
	FB.getLoginStatus(function(response) {
		statusChangeCallback(response);
	});
}

function verificaLogin() {
	FB.getLoginStatus(function(response) {
		statusChangeCallback(response);
	});
}

function logoutFB() {

	FB.getLoginStatus(function(ret) {
		/// are they currently logged into Facebook?
		if (ret.authResponse) {
			//they were authed so do the logout
			FB.logout(function(response) {
				AWS.config.credentials.clearCachedId();
				window.location = "index.html";
				localStorage.removeItem("login");
			});
		} else {
			///do something if they aren't logged in
			//or just get rid of this if you don't need to do something if they weren't logged in
			console.log("Não estava logado");
		}
	});

}

function loginFB() {

	FB.login(function(response) {

		// Check if the user logged in successfully.
		if (response.authResponse) {

			var token = response.authResponse.accessToken;
			// Add the Facebook access token to the Cognito credentials login map.
			AWS.config.credentials = new AWS.CognitoIdentityCredentials({
				IdentityPoolId : 'us-east-1:369a7924-94e2-47b5-8632-1850a69c0d4c',
				Logins : {
					'graph.facebook.com' : response.authResponse.accessToken
				}
			});

			// Obtain AWS credentials

			AWS.config.credentials.get(function() {
				FB.api('/me', function(response) {
					window.location = "first.html?ClientID=" + response.name + "&question=1&Token=" + token + "&login=facebook";
					localStorage.setItem("login", "facebook");
				});
			});

		} else {
			console.log('There was a problem logging you in.');
		}

	});

}

window.fbAsyncInit = function() {
	FB.init({
		appId : '671134936373659',
		cookie : true,
		xfbml : true,
		version : 'v2.6'
	});

	AWS.config.region = 'us-east-1';
	AWS.config.credentials = new AWS.CognitoIdentityCredentials({
		IdentityPoolId : 'us-east-1:369a7924-94e2-47b5-8632-1850a69c0d4c',
	});

};

// Load the SDK asynchronously
( function(d, s, id) {
		var js,
		    fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id))
			return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
